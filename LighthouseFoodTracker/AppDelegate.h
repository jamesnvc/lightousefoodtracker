//
//  AppDelegate.h
//  LighthouseFoodTracker
//
//  Created by James Cash on 25-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

