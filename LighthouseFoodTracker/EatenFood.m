//
//  EatenFood.m
//  LighthouseFoodTracker
//
//  Created by James Cash on 25-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import "EatenFood.h"


@implementation EatenFood

@dynamic name;
@dynamic calories;
@dynamic protein;
@dynamic carbs;
@dynamic fat;
@dynamic eatenAt;

- (NSString *)dateEaten
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.timeStyle = NSDateFormatterNoStyle;
    formatter.dateStyle = NSDateFormatterShortStyle;
    return [formatter stringFromDate:self.eatenAt];
}

@end