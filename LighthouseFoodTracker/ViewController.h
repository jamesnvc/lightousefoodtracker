//
//  ViewController.h
//  LighthouseFoodTracker
//
//  Created by James Cash on 25-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITextField *foodNameField;
@property (weak, nonatomic) IBOutlet UITextField *caloriesField;
@property (weak, nonatomic) IBOutlet UITextField *proteinField;
@property (weak, nonatomic) IBOutlet UITextField *carbsField;
@property (weak, nonatomic) IBOutlet UITextField *fatField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

