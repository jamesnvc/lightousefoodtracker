//
//  EatenFood.h
//  LighthouseFoodTracker
//
//  Created by James Cash on 25-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EatenFood : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * calories;
@property (nonatomic, retain) NSNumber * protein;
@property (nonatomic, retain) NSNumber * carbs;
@property (nonatomic, retain) NSNumber * fat;
@property (nonatomic, retain) NSDate   * eatenAt;
@property (nonatomic, readonly) NSDate * dateEaten;


@end
