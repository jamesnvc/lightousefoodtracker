//
//  EatenFoodStore.h
//  LighthouseFoodTracker
//
//  Created by James Cash on 25-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreData;

@interface EatenFoodStore : NSObject

+ (EatenFoodStore *)defaultStore;
- (void)saveEatenFood:(NSString *)name withMacros:(NSDictionary *)macros;
- (NSFetchedResultsController *)eatenFoodFetchController;

@end
