//
//  ViewController.m
//  LighthouseFoodTracker
//
//  Created by James Cash on 25-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "EatenFoodStore.h"
#import "EatenFood.h"

@interface ViewController () <NSFetchedResultsControllerDelegate> {
    NSFetchedResultsController *ctrl;
}

@end

@implementation ViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    ctrl = [[EatenFoodStore defaultStore] eatenFoodFetchController];
    ctrl.delegate = self;
    NSError *err;
    if (![ctrl performFetch:&err]) {
        NSLog(@"error: %@", err.localizedDescription);
        abort();
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)addFood:(id)sender {
    [[EatenFoodStore defaultStore]
     saveEatenFood:self.foodNameField.text
     withMacros:@{@"calories": @([self.caloriesField.text integerValue]),
                  @"protein": @([self.proteinField.text integerValue]),
                  @"carbs": @([self.carbsField.text integerValue]),
                  @"fat": @([self.fatField.text integerValue])}];
    self.caloriesField.text = @"";
    self.proteinField.text = @"";
    self.carbsField.text = @"";
    self.fatField.text = @"";
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [ctrl sections].count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self numberOfSectionsInTableView:nil] == 0) {
        return 0;
    }
    return [[[ctrl sections] objectAtIndex:section] numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"eatenFoodCell"];
    EatenFood *food = [ctrl objectAtIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"%@kcal %@p/%@f/%@c", food.calories, food.protein, food.carbs, food.fat];
    return cell;
}

#pragma mark - NSFetchresultscontrollerdelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableview = self.tableView;
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [tableview insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [tableview deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            NSLog(@"Row changed %@", indexPath);
            break;
        case NSFetchedResultsChangeMove:
            [tableview deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableview insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                                withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                                withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}


@end
