//
//  EatenFoodStore.m
//  LighthouseFoodTracker
//
//  Created by James Cash on 25-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import "EatenFoodStore.h"
#import "EatenFood.h"

static EatenFoodStore *defaultStore = nil;

@interface EatenFoodStore ()
@property (strong,nonatomic) NSManagedObjectContext *moc;
@property (nonatomic,strong) NSManagedObjectModel *mom;
@property (nonatomic,strong) NSPersistentStoreCoordinator *psc;
@end

@implementation EatenFoodStore

#pragma mark - Lifecycle

+ (EatenFoodStore *)defaultStore
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!defaultStore) {
            defaultStore = [[super allocWithZone:NULL] init];
        }
    });
    return defaultStore;
}

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    return [self defaultStore];
}

- (instancetype)init
{
    if (defaultStore) {
        return defaultStore;
    }
    self = [super init];
    return self;
}

#pragma mark - Core Data stack


- (NSManagedObjectModel *)mom
{
    if (_mom) {
        return _mom;
    }
    _mom = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _mom;
}

- (NSPersistentStoreCoordinator *)psc
{
    if (_psc) {
        return _psc;
    }
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *storeURL = [[[fm URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:@"eatenfoodstore.sqlite"];
    NSError *err;
    _psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.mom];
    if (![_psc addPersistentStoreWithType:NSSQLiteStoreType
                            configuration:nil
                                      URL:storeURL
                                  options:@{NSInferMappingModelAutomaticallyOption: @YES}
                                    error:&err]) {
        NSLog(@"failed to create store: %@, %@", err.localizedDescription, err.userInfo);
        abort();
    }
    return _psc;
}

- (NSManagedObjectContext *)moc
{
    if (_moc) {
        return _moc;
    }
    NSPersistentStoreCoordinator *coord = self.psc;
    if (coord) {
        _moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_moc setPersistentStoreCoordinator:coord];
    }
    return _moc;
}

#pragma mark - Creating items

- (void)saveEatenFood:(NSString *)name withMacros:(NSDictionary *)macros
{
    NSManagedObjectContext *tmpCtx = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    tmpCtx.parentContext = self.moc;
    [tmpCtx performBlock:^{
        EatenFood *eaten = [[EatenFood alloc] initWithEntity:[NSEntityDescription entityForName:@"EatenFood"
                                                                         inManagedObjectContext:tmpCtx]
                              insertIntoManagedObjectContext:tmpCtx];
        eaten.name = name;
        eaten.calories = macros[@"calories"];
        eaten.protein = macros[@"protein"];
        eaten.carbs = macros[@"carbs"];
        eaten.fat = macros[@"fat"];
        eaten.eatenAt = [NSDate date];

        NSError *err;
        if (![tmpCtx save:&err]) {
            NSLog(@"Error saving eaten %@", err.localizedDescription);
            return;
        }
        NSLog(@"Saved in tmp");
        [self.moc performBlock:^{
            NSError *err;
            if (![self.moc save:&err]) {
                NSLog(@"Error saving eaten %@", err.localizedDescription);
                return;
            }
            NSLog(@"Saved in main");
        }];

    }];
}

#pragma mark - fetching

- (NSFetchedResultsController *)eatenFoodFetchController
{
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    NSEntityDescription *eaten = [NSEntityDescription entityForName:@"EatenFood" inManagedObjectContext:self.moc];
    [req setEntity:eaten];
    [req setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"eatenAt" ascending:NO]]];
    NSFetchedResultsController *ctrl = [[NSFetchedResultsController alloc]
                                        initWithFetchRequest:req
                                        managedObjectContext:self.moc
                                        sectionNameKeyPath:@"dateEaten"
                                        cacheName:@"Root"];
    return ctrl;
}

@end
